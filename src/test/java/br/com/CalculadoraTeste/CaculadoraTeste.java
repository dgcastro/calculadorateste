package br.com.CalculadoraTeste;

import br.com.calculadora.Calculadora;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class CaculadoraTeste {

    private Calculadora calculadora;
    @BeforeEach
    public void setUp(){
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDeDoisNumerosInteiros(){
        int resultado = calculadora.soma(1,2);
        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testaASomaDeNoisNumerosFlutuantes(){
        double resultado = calculadora.soma(2.3, 3.4);
        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteiros(){
        int resultado = calculadora.divisao(1,2);
        Assertions.assertEquals(0,resultado);
    }

    @Test
    public void testaADivisaoDeUmNumerosInteirosNegativos(){
        int resultado = calculadora.divisao(-13, 5);
        Assertions.assertEquals(-2, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosInteirosNegativos(){
        int resultado = calculadora.divisao(-13, -5);
        Assertions.assertEquals(2, resultado);
    }

    @Test
    public void testaADivisaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.divisao(7.26, 2.2);
        Assertions.assertEquals(3.30, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosInteiros(){
        int resultado = calculadora.multiplicacao(7,9);
        Assertions.assertEquals(63, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeUmNumerosInteirosNegativos(){
        int resultado = calculadora.multiplicacao(-5,5);
        Assertions.assertEquals(-25,resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosNegativos(){
        double resultado = calculadora.multiplicacao(-5, -2.5);
        Assertions.assertEquals(12.50, resultado);
    }

    @Test
    public void testaAMultiplicacaoDeDoisNumerosFlutuantes(){
        double resultado = calculadora.multiplicacao(5.5,2.5);
        Assertions.assertEquals(13.75, resultado);

    }
}

