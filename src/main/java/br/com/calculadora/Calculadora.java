package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculadora {
    public Calculadora(){

    }

    public int soma(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public double soma(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero+segundoNumero;
        return ajusteDecimal(resultado);
    }

    public int divisao(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero/segundoNumero;
        return resultado;
    }

    public double divisao(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero/segundoNumero;
        return ajusteDecimal(resultado);
    }

    public int multiplicacao(int primeiroNumero, int segundoNumero){
        int resultado = primeiroNumero*segundoNumero;
        return resultado;
    }

    public double multiplicacao(double primeiroNumero, double segundoNumero){
        double resultado = primeiroNumero*segundoNumero;
        return ajusteDecimal(resultado);
    }

    public double ajusteDecimal(double valor){
        BigDecimal bigDecimal = new BigDecimal(valor).setScale(2,RoundingMode.HALF_EVEN);
        return bigDecimal.doubleValue();
    }

}
